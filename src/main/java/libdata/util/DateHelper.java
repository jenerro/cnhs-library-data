package libdata.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("MM-dd-yyyy");
    
    private static final SimpleDateFormat FORMATTER_WITH_TIME = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

    public static String getYearToday() {
        String today = FORMATTER.format(new Date());
        return today.split("-")[2];
    }

    public static String getDateToday() {
        String today = FORMATTER.format(new Date());
        return today.split("-")[1];
    }

    public static String getMonthToday() {
        String today = FORMATTER.format(new Date());
        return today.split("-")[2];
    }

    public static String getToday() {
        String today = FORMATTER.format(new Date());
        return today;
    }

    public static String format(Date date) {
        return FORMATTER.format(date);
    }
    public static String getTodayWithTime(){
        return FORMATTER_WITH_TIME.format(new Date());
    }
}