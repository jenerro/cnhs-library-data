package libdata.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

public class SpecificationBuilder<T> {

    private List<SearchCriteria> params;

    public SpecificationBuilder(String searchParam) {
        params = new ArrayList<>();
        addParamToCriteria(searchParam);
    }

    private void addParamToCriteria(String searchParam) {
        String[] values = searchParam.split(",");
        for (String filter : values) {
            String[] validFilter = hasValidOperators(filter);
            if (validFilter.length > 0) {
                params.add(new SearchCriteria(validFilter[0], validFilter[1], validFilter[2]));
            }
        }
    }

    public Specification<T> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<T>> specs = new ArrayList<Specification<T>>();
        for (SearchCriteria criteria : params) {
            specs.add(new Specification<T>() {

                @Override
                public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                    if (criteria.getOperation().equalsIgnoreCase(":")) {

                        if (root.get(criteria.getKey()).getJavaType() == String.class) {
                            return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
                        }
                    } else if (criteria.getOperation().equalsIgnoreCase(">")) {

                        if (root.get(criteria.getKey()).getJavaType() == String.class) {
                            builder.greaterThanOrEqualTo(root.<String>get(criteria.getKey()), criteria.getValue().toString());
                        }

                        if (root.get(criteria.getKey()).getJavaType() == int.class) {
                            return builder.greaterThanOrEqualTo(root.<Integer>get(criteria.getKey()),
                                                                Integer.parseInt(criteria.getValue().toString()));
                        }

                    } else if (criteria.getOperation().equalsIgnoreCase("<")) {

                        if (root.get(criteria.getKey()).getJavaType() == String.class) {
                            builder.lessThanOrEqualTo(root.<String>get(criteria.getKey()), criteria.getValue().toString());
                        }

                        if (root.get(criteria.getKey()).getJavaType() == int.class) {
                            return builder.lessThanOrEqualTo(root.<Integer>get(criteria.getKey()), Integer.parseInt(criteria.getValue().toString()));
                        }

                    } else if (criteria.getOperation().equalsIgnoreCase("=")) {
                        if (root.get(criteria.getKey()).getJavaType() == String.class) {
                            return builder.equal(root.<String>get(criteria.getKey()), criteria.getValue());
                        }

                        if (root.get(criteria.getKey()).getJavaType() == int.class) {
                            return builder.equal(root.<Integer>get(criteria.getKey()), Integer.parseInt(criteria.getValue().toString()));
                        }

                        if (root.get(criteria.getKey()).getJavaType() == boolean.class) {
                            return builder.equal(root.<Boolean>get(criteria.getKey()), Boolean.parseBoolean(criteria.getValue().toString()));
                        }
                    }
                    return null;
                }
            });
        }

        Specification<T> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private String[] hasValidOperators(String filter) {
        if (filter.contains("=")) {
            String column = filter.split("=")[0];
            String value = filter.split("=")[1];
            return new String[] { column, "=", value };
        }
        if (filter.contains(":")) {
            String column = filter.split(":")[0];
            String value = filter.split(":")[1];
            return new String[] { column, ":", value };
        }
        if (filter.contains("<")) {
            String column = filter.split("<")[0];
            String value = filter.split("<")[1];
            return new String[] { column, "<", value };
        }
        if (filter.contains(">")) {
            String column = filter.split(">")[0];
            String value = filter.split(">")[1];
            return new String[] { column, ">", value };
        }
        return new String[] {};
    }

}