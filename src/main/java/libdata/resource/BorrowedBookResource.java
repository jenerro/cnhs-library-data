package libdata.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import libdata.model.BorrowedBook;
import libdata.service.BorrowedBookService;
import libdata.util.SpecificationBuilder;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@RestController
@RequestMapping("/borrowedbooks")
public class BorrowedBookResource {
	
	@Autowired
	private BorrowedBookService borrowedBookService;
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody BorrowedBook save(@RequestBody BorrowedBook borrowedBook){
		return borrowedBookService.save(borrowedBook);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public @ResponseBody BorrowedBook update(@RequestBody BorrowedBook borrowedBook){
		return borrowedBookService.update(borrowedBook);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public @ResponseBody BorrowedBook findOne(@PathVariable("id") long id){
		return borrowedBookService.findOne(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/batch")
	public @ResponseBody List<BorrowedBook> batchSave(@RequestBody List<BorrowedBook> borrowedBooks){
        return borrowedBookService.save(borrowedBooks);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/batch")
	public @ResponseBody List<BorrowedBook> update(@RequestBody List<BorrowedBook> borrowedBooks){
		return borrowedBookService.update(borrowedBooks);
	}

	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<BorrowedBook> findAll(@RequestParam(value="search", required=false) String search){
		if (search != null) {
			SpecificationBuilder<BorrowedBook> builder = new SpecificationBuilder<>(search);
	        Specification<BorrowedBook> borrowedBookSpecification = builder.build();
			return borrowedBookService.findAll(borrowedBookSpecification);
		}
		return borrowedBookService.findAll();
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public void delete(@RequestBody long id){
		borrowedBookService.delete(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/batch")
	public void batchDelete(@RequestBody List<BorrowedBook> borrowedBooks){
		borrowedBookService.deleteInBatch(borrowedBooks);
	}
}
