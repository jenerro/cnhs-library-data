package libdata.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import libdata.model.Borrower;
import libdata.service.BorrowerService;
import libdata.util.SpecificationBuilder;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@RestController
@RequestMapping("/borrowers")
public class BorrowerResource {
	
	@Autowired
	private BorrowerService borrowerService;
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody Borrower save(@RequestBody Borrower borrower){
		return borrowerService.save(borrower);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public @ResponseBody Borrower update(@RequestBody Borrower borrower){
		return borrowerService.update(borrower);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public @ResponseBody Borrower findOne(@PathVariable("id") long id){
		return borrowerService.findOne(id);
	}

	@RequestMapping(method=RequestMethod.POST, value="/batch")
	public @ResponseBody List<Borrower> batchSave(@RequestBody List<Borrower> borrowers){
        return borrowerService.save(borrowers);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/batch")
	public @ResponseBody List<Borrower> update(@RequestBody List<Borrower> borrowers){
		return borrowerService.update(borrowers);
	}

	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Borrower> findAll(@RequestParam(value="search", required=false) String search){
		if (search != null) {
			SpecificationBuilder<Borrower> builder = new SpecificationBuilder<>(search);
	        Specification<Borrower> borrowerSpecification = builder.build();
			return borrowerService.findAll(borrowerSpecification);
		}
		return borrowerService.findAll();
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public void delete(@RequestBody long id){
		borrowerService.delete(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/batch")
	public void batchDelete(@RequestBody List<Borrower> borrowers){
		borrowerService.deleteInBatch(borrowers);
	}
}
