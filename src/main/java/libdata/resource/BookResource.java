package libdata.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import libdata.model.Book;
import libdata.service.BookService;
import libdata.util.SpecificationBuilder;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@RestController
@RequestMapping("/books")
public class BookResource {
	
	@Autowired
	private BookService bookService;
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody Book save(@RequestBody Book book){
		return bookService.save(book);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public @ResponseBody Book update(@RequestBody Book book){
		return bookService.update(book);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public @ResponseBody Book findOne(@PathVariable("id") long id){
		return bookService.findOne(id);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/referencenumber/{referencenumber}")
	public @ResponseBody Book findByReferenceNumber(@PathVariable("referencenumber") String referenceNumber){
		return bookService.findByReferenceNumber(referenceNumber);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/batch")
	public @ResponseBody List<Book> batchSave(@RequestBody List<Book> books){
        return bookService.save(books);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/batch")
	public @ResponseBody List<Book> update(@RequestBody List<Book> books){
		return bookService.update(books);
	}

	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Book> findAll(@RequestParam(value="search", required=false) String search){
		if (search != null) {
			SpecificationBuilder<Book> builder = new SpecificationBuilder<>(search);
	        Specification<Book> bookSpecification = builder.build();
			return bookService.findAll(bookSpecification);
		}
		return bookService.findAll();
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public void delete(@RequestBody long id){
		bookService.delete(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/batch")
	public void batchDelete(@RequestBody List<Book> books){
		bookService.deleteInBatch(books);
	}
}
