package libdata.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import libdata.model.Section;
import libdata.service.SectionService;
import libdata.util.SpecificationBuilder;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@RestController
@RequestMapping("/sections")
public class SectionResource {
	
	@Autowired
	private SectionService sectionService;
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody Section save(@RequestBody Section section){
		return sectionService.save(section);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public @ResponseBody Section update(@RequestBody Section section){
		return sectionService.update(section);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public @ResponseBody Section findOne(@PathVariable("id") long id){
		return sectionService.findOne(id);
	}

	@RequestMapping(method=RequestMethod.POST, value="/batch")
	public @ResponseBody List<Section> batchSave(@RequestBody List<Section> sections){
        return sectionService.save(sections);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/batch")
	public @ResponseBody List<Section> update(@RequestBody List<Section> sections){
		return sectionService.update(sections);
	}

	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Section> findAll(@RequestParam(value="search", required=false) String search){
		if (search != null) {
			SpecificationBuilder<Section> builder = new SpecificationBuilder<>(search);
	        Specification<Section> sectionSpecification = builder.build();
			return sectionService.findAll(sectionSpecification);
		}
		return sectionService.findAll();
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public void delete(@RequestBody long id){
		sectionService.delete(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/batch")
	public void batchDelete(@RequestBody List<Section> sections){
		sectionService.deleteInBatch(sections);
	}
}
