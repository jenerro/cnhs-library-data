package libdata.resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 *
 */
@RestController
public class HelloResource {

    @RequestMapping(value="/hello", method=RequestMethod.GET)
    public String index() {
        return "Greetings from Spring Boot!";
    }

}
