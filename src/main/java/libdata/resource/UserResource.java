package libdata.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import libdata.model.User;
import libdata.service.UserService;
import libdata.util.SpecificationBuilder;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@RestController
@RequestMapping("/users")
public class UserResource {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody User save(@RequestBody User user){
		return userService.save(user);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public @ResponseBody User update(@RequestBody User user){
		return userService.update(user);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public @ResponseBody User findOne(@PathVariable("id") long id){
		return userService.findOne(id);
	}

	@RequestMapping(method=RequestMethod.POST, value="/batch")
	public @ResponseBody List<User> batchSave(@RequestBody List<User> users){
        return userService.save(users);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/batch")
	public @ResponseBody List<User> update(@RequestBody List<User> users){
		return userService.update(users);
	}

	@RequestMapping(method=RequestMethod.GET, produces="application/json")
	public @ResponseBody List<User> findAll(@RequestParam(value="search", required=false) String search){
		if (search != null) {
			SpecificationBuilder<User> builder = new SpecificationBuilder<>(search);
	        Specification<User> userSpecification = builder.build();
			return userService.findAll(userSpecification);
		}
		return userService.findAll();
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public void delete(@RequestBody long id){
		userService.delete(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/batch")
	public void batchDelete(@RequestBody List<User> users){
		userService.deleteInBatch(users);
	}
}
