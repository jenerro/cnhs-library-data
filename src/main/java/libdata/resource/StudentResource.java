package libdata.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import libdata.model.Student;
import libdata.service.StudentService;
import libdata.util.SpecificationBuilder;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@RestController
@RequestMapping("/students")
public class StudentResource {
	
	@Autowired
	private StudentService studentService;
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody Student save(@RequestBody Student student){
		return studentService.save(student);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public @ResponseBody Student update(@RequestBody Student student){
		return studentService.update(student);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public @ResponseBody Student findOne(@PathVariable("id") long id){
		return studentService.findOne(id);
	}

	@RequestMapping(method=RequestMethod.POST, value="/batch")
	public @ResponseBody List<Student> batchSave(@RequestBody List<Student> students){
        return studentService.save(students);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/batch")
	public @ResponseBody List<Student> update(@RequestBody List<Student> students){
		return studentService.update(students);
	}

	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Student> findAll(@RequestParam(value="search", required=false) String search){
		if (search != null) {
			SpecificationBuilder<Student> builder = new SpecificationBuilder<>(search);
	        Specification<Student> studentSpecification = builder.build();
			return studentService.findAll(studentSpecification);
		}
		return studentService.findAll();
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public void delete(@RequestBody long id){
		studentService.delete(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/batch")
	public void batchDelete(@RequestBody List<Student> students){
		studentService.deleteInBatch(students);
	}
}
