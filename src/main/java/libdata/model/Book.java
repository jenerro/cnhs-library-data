package libdata.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@Entity
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String referenceNumber;
	private String author;
	private String title;
	private String edition;
	private String publisher;
	private String yearPublished;
	private String numberOfPages;
	private String copyNumber;
	private String dateReceived;
	private String sourceOfFund;
	private String remarks;
	private String dateCreated;
	private String createdBy;
	private String dateModified;
	private String modifiedBy;

	public Book() {
	}

	public Book(long id, String referenceNumber, String author, String title, String edition, String publisher,
			String yearPublished, String numberOfPages, String copyNumber, String dateReceived, String sourceOfFund,
			String remarks, String dateCreated, String createdBy, String dateModified, String modifiedBy) {
		this.id = id;
		this.referenceNumber = referenceNumber;
		this.author = author;
		this.title = title;
		this.edition = edition;
		this.publisher = publisher;
		this.yearPublished = yearPublished;
		this.numberOfPages = numberOfPages;
		this.copyNumber = copyNumber;
		this.dateReceived = dateReceived;
		this.sourceOfFund = sourceOfFund;
		this.remarks = remarks;
		this.dateCreated = dateCreated;
		this.createdBy = createdBy;
		this.dateModified = dateModified;
		this.modifiedBy = modifiedBy;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getYearPublished() {
		return yearPublished;
	}

	public void setYearPublished(String yearPublished) {
		this.yearPublished = yearPublished;
	}

	public String getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(String numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public String getCopyNumber() {
		return copyNumber;
	}

	public void setCopyNumber(String copyNumber) {
		this.copyNumber = copyNumber;
	}

	public String getDateReceived() {
		return dateReceived;
	}

	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}

	public String getSourceOfFund() {
		return sourceOfFund;
	}

	public void setSourceOfFund(String sourceOfFund) {
		this.sourceOfFund = sourceOfFund;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDateModified() {
		return dateModified;
	}

	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		return String.format(
				"Book[id=%d, referenceNumber='%s', author='%s', title='%s', edition='%s', publisher='%s', yearPublished='%s', numberOfPages='%s', copyNumber='%s', dateReceived='%s', sourceOfFund='%s', remarks='%s', dateCreated='%s', createdBy='%s', dateModified='%s', modifiedBy='%s']",
				id, referenceNumber, author, title, edition, publisher, yearPublished, numberOfPages, copyNumber, dateReceived, sourceOfFund, remarks, dateCreated, createdBy, dateModified, modifiedBy);
	}

}