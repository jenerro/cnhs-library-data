package libdata.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@Entity
public class BorrowedBook {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String dateBorrowed;
    private String dateReturned;
    private int status;
    private long borrowerId;
    private long bookId;
	private String dateCreated;
	private String createdBy;
	private String dateModified;
	private String modifiedBy;
    
	public BorrowedBook() {}

	public BorrowedBook(long id, String dateBorrowed, String dateReturned, int status, long borrowerId, long bookId,
			String dateCreated, String createdBy, String dateModified, String modifiedBy) {
		this.id = id;
		this.dateBorrowed = dateBorrowed;
		this.dateReturned = dateReturned;
		this.status = status;
		this.borrowerId = borrowerId;
		this.bookId = bookId;
		this.dateCreated = dateCreated;
		this.createdBy = createdBy;
		this.dateModified = dateModified;
		this.modifiedBy = modifiedBy;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDateBorrowed() {
		return dateBorrowed;
	}

	public void setDateBorrowed(String dateBorrowed) {
		this.dateBorrowed = dateBorrowed;
	}

	public String getDateReturned() {
		return dateReturned;
	}

	public void setDateReturned(String dateReturned) {
		this.dateReturned = dateReturned;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(long borrowerId) {
		this.borrowerId = borrowerId;
	}

	public long getBookId() {
		return bookId;
	}

	public void setBookId(long bookId) {
		this.bookId = bookId;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDateModified() {
		return dateModified;
	}

	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
}
