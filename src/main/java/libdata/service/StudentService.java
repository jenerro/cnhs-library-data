package libdata.service;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import libdata.model.Student;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface StudentService {
	
	Student save(Student student);
	
	Student update(Student student);
	
	Student findOne(long id);
	
	List<Student> save(List<Student> students);
	
	List<Student> update(List<Student> students);
	
	List<Student> findAll();
	
	List<Student> findAll(Specification<Student> studentSpecification);
	
	void delete(long id);
	
	void deleteInBatch(List<Student> students);
}
