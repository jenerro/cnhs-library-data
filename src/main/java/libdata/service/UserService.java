package libdata.service;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import libdata.model.User;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface UserService {
	
	User save(User user);
	
	User update(User user);
	
	User findOne(long id);
	
	List<User> save(List<User> users);
	
	List<User> update(List<User> users);
	
	List<User> findAll();
	
	List<User> findAll(Specification<User> userSpecification);
	
	void delete(long id);
	
	void deleteInBatch(List<User> users);
}
