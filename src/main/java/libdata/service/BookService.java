package libdata.service;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import libdata.model.Book;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface BookService {
	
	Book save(Book book);
	
	Book update(Book book);
	
	Book findOne(long id);
	
	Book findByReferenceNumber(String referenceNumber);
	
	List<Book> save(List<Book> books);
	
	List<Book> update(List<Book> books);
	
	List<Book> findAll();
	
	List<Book> findAll(Specification<Book> bookSpecification);
	
	void delete(long id);
	
	void deleteInBatch(List<Book> books);
}
