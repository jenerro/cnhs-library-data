package libdata.service;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import libdata.model.Section;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface SectionService {
	
	Section save(Section section);
	
	Section update(Section section);
	
	Section findOne(long id);
	
	List<Section> save(List<Section> sections);
	
	List<Section> update(List<Section> sections);
	
	List<Section> findAll();
	
	List<Section> findAll(Specification<Section> sectionSpecification);
	
	void delete(long id);
	
	void deleteInBatch(List<Section> sections);
}
