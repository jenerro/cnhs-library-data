package libdata.service;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import libdata.model.Borrower;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface BorrowerService {
	
	Borrower save(Borrower borrower);
	
	Borrower update(Borrower borrower);
	
	Borrower findOne(long id);
	
	List<Borrower> save(List<Borrower> borrowers);
	
	List<Borrower> update(List<Borrower> borrowers);
	
	List<Borrower> findAll();
	
	List<Borrower> findAll(Specification<Borrower> borrowerSpecification);
	
	void delete(long id);
	
	void deleteInBatch(List<Borrower> borrowers);
}
