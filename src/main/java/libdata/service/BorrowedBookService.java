package libdata.service;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import libdata.model.BorrowedBook;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface BorrowedBookService {
	
	BorrowedBook save(BorrowedBook borrowedBook);
	
	BorrowedBook update(BorrowedBook borrowedBook);
	
	BorrowedBook findOne(long id);
	
	List<BorrowedBook> save(List<BorrowedBook> borrowedBooks);
	
	List<BorrowedBook> update(List<BorrowedBook> borrowedBooks);
	
	List<BorrowedBook> findAll();
	
	List<BorrowedBook> findAll(Specification<BorrowedBook> borrowedBookSpecification);
	
	void delete(long id);
	
	void deleteInBatch(List<BorrowedBook> borrowedBooks);
}
