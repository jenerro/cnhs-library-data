package libdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import libdata.model.Borrower;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface BorrowerRepository extends JpaRepository<Borrower, Long>, JpaSpecificationExecutor<Borrower> {
	Borrower findById(long id);
}