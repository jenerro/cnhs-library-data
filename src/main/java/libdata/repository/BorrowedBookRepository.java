package libdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import libdata.model.BorrowedBook;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface BorrowedBookRepository extends JpaRepository<BorrowedBook, Long>, JpaSpecificationExecutor<BorrowedBook> {
	BorrowedBook findById(long id);
}