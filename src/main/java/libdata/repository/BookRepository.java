package libdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import libdata.model.Book;

/**
 * @author Ace Jenerro Baluyot
 *
 */

//@RepositoryRestResource(collectionResourceRel = "book", path = "book")
public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {
	Book findById(long id);
	Book findByReferenceNumber(String referenceNumber);
}
