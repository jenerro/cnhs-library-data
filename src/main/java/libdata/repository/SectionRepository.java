package libdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import libdata.model.Section;

/**
 * @author Ace Jenerro Baluyot
 *
 */
public interface SectionRepository extends JpaRepository<Section, Long>, JpaSpecificationExecutor<Section> {
	Section findById(long id);
}