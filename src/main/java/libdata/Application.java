package libdata;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

//import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

//import libdata.model.Book;
//import libdata.repository.BookRepository;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class Application {
	
	//private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
    // Swagger Configuration here .... //
    @Bean
    public Docket api() {

        return new Docket(DocumentationType.SWAGGER_2).select()
                                                      .apis(RequestHandlerSelectors.basePackage("libdata"))
                                                      .paths(PathSelectors.any())
                                                      .build()
                                                      .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo("Citrus National High School Library Data",
                                      "Stateless service.",
                                      "v0.1.0",
                                      "Terms of service",
                                      "acejenerrobaluyot@gmail.com",
                                      null,
                                      null);
        return apiInfo;
    }
    
	/*@Bean
	public CommandLineRunner demo(BookRepository repository) {
		return (args) -> {
			// test
			log.info("");
		};
	}*/
}
