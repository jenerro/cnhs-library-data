package libdata.serviceImplemention;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import libdata.model.Book;
import libdata.repository.BookRepository;
import libdata.service.BookService;
import libdata.util.DateHelper;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@Service
@Transactional
public class BookServiceImp implements BookService {

	@Autowired
	private BookRepository bookRepository;

	@Override
	public Book save(Book book) {
		Book returnObj = null;
		book.setDateCreated(DateHelper.getTodayWithTime());
		book.setModifiedBy(null);
		book.setDateModified(null);
		returnObj = bookRepository.save(book);
		return returnObj;
	}

	@Override
	public Book update(Book book) {
		Book returnObj = null;
		book.setDateModified(DateHelper.getTodayWithTime());
		returnObj = bookRepository.save(book);
		return returnObj;
	}

	@Override
	public Book findOne(long id) {
		Book returnObj = null;
		returnObj = bookRepository.findById(id);
		return returnObj;
	}

	@Override
	public Book findByReferenceNumber(String referenceNumber) {
		Book returnObj = null;
		returnObj = bookRepository.findByReferenceNumber(referenceNumber);
		return returnObj;
	}

	@Override
	public List<Book> save(List<Book> books) {
		List<Book> returnObj = null;
		books.stream().forEach(b -> {
			b.setDateCreated(DateHelper.getTodayWithTime());
			b.setDateModified(null);
		});
		returnObj = bookRepository.save(books);
		return returnObj;
	}

	@Override
	public List<Book> update(List<Book> books) {
		List<Book> returnObj = null;
		books.stream().forEach(b -> b.setDateModified(DateHelper.getTodayWithTime()));
		returnObj = bookRepository.save(books);
		return returnObj;
	}

	@Override
	public List<Book> findAll() {
		List<Book> books = null;
		books = bookRepository.findAll();
		return books;
	}

	@Override
	public List<Book> findAll(Specification<Book> bookSpecification) {
		List<Book> books = null;
		books = bookRepository.findAll(bookSpecification);
		return books;
	}

	@Override
	public void delete(long id) {
		bookRepository.delete(id);
	}

	@Override
	public void deleteInBatch(List<Book> books) {
		bookRepository.deleteInBatch(books);
	}

}
