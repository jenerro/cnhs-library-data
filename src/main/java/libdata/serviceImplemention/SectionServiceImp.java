package libdata.serviceImplemention;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import libdata.model.Section;
import libdata.repository.SectionRepository;
import libdata.service.SectionService;
import libdata.util.DateHelper;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@Service
@Transactional
public class SectionServiceImp implements SectionService {

	@Autowired
	private SectionRepository sectionRepository;

	@Override
	public Section save(Section section) {
		Section returnObj = null;
		section.setDateCreated(DateHelper.getTodayWithTime());
		section.setModifiedBy(null);
		section.setDateModified(null);
		returnObj = sectionRepository.save(section);
		return returnObj;
	}

	@Override
	public Section update(Section section) {
		Section returnObj = null;
		section.setDateModified(DateHelper.getTodayWithTime());
		returnObj = sectionRepository.save(section);
		return returnObj;
	}

	@Override
	public Section findOne(long id) {
		Section returnObj = null;
		returnObj = sectionRepository.findById(id);
		return returnObj;
	}

	@Override
	public List<Section> save(List<Section> sections) {
		List<Section> returnObj = null;
		sections.stream().forEach(b -> {
			b.setDateCreated(DateHelper.getTodayWithTime());
			b.setDateModified(null);
		});
		returnObj = sectionRepository.save(sections);
		return returnObj;
	}

	@Override
	public List<Section> update(List<Section> sections) {
		List<Section> returnObj = null;
		sections.stream().forEach(b -> b.setDateModified(DateHelper.getTodayWithTime()));
		returnObj = sectionRepository.save(sections);
		return returnObj;
	}

	@Override
	public List<Section> findAll() {
		List<Section> sections = null;
		sections = sectionRepository.findAll();
		return sections;
	}

	@Override
	public List<Section> findAll(Specification<Section> sectionSpecification) {
		List<Section> sections = null;
		sections = sectionRepository.findAll(sectionSpecification);
		return sections;
	}

	@Override
	public void delete(long id) {
		sectionRepository.delete(id);
	}

	@Override
	public void deleteInBatch(List<Section> sections) {
		sectionRepository.deleteInBatch(sections);
	}

}
