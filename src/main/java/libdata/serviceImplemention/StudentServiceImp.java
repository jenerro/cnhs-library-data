package libdata.serviceImplemention;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import libdata.model.Student;
import libdata.repository.StudentRepository;
import libdata.service.StudentService;
import libdata.util.DateHelper;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@Service
@Transactional
public class StudentServiceImp implements StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public Student save(Student student) {
		Student returnObj = null;
		student.setDateCreated(DateHelper.getTodayWithTime());
		student.setModifiedBy(null);
		student.setDateModified(null);
		returnObj = studentRepository.save(student);
		return returnObj;
	}

	@Override
	public Student update(Student student) {
		Student returnObj = null;
		student.setDateModified(DateHelper.getTodayWithTime());
		returnObj = studentRepository.save(student);
		return returnObj;
	}

	@Override
	public Student findOne(long id) {
		Student returnObj = null;
		returnObj = studentRepository.findById(id);
		return returnObj;
	}

	@Override
	public List<Student> save(List<Student> students) {
		List<Student> returnObj = null;
		students.stream().forEach(b -> {
			b.setDateCreated(DateHelper.getTodayWithTime());
			b.setDateModified(null);
		});
		returnObj = studentRepository.save(students);
		return returnObj;
	}

	@Override
	public List<Student> update(List<Student> students) {
		List<Student> returnObj = null;
		students.stream().forEach(b -> b.setDateModified(DateHelper.getTodayWithTime()));
		returnObj = studentRepository.save(students);
		return returnObj;
	}

	@Override
	public List<Student> findAll() {
		List<Student> students = null;
		students = studentRepository.findAll();
		return students;
	}

	@Override
	public List<Student> findAll(Specification<Student> studentSpecification) {
		List<Student> students = null;
		students = studentRepository.findAll(studentSpecification);
		return students;
	}

	@Override
	public void delete(long id) {
		studentRepository.delete(id);
	}

	@Override
	public void deleteInBatch(List<Student> students) {
		studentRepository.deleteInBatch(students);
	}

}
