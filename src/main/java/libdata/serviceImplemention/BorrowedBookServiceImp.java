package libdata.serviceImplemention;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import libdata.model.BorrowedBook;
import libdata.repository.BorrowedBookRepository;
import libdata.service.BorrowedBookService;
import libdata.util.DateHelper;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@Service
@Transactional
public class BorrowedBookServiceImp implements BorrowedBookService{

	@Autowired
	private BorrowedBookRepository borrowedBookRepository;

	@Override
	public BorrowedBook save(BorrowedBook borrowedBook) {
		BorrowedBook returnObj = null;
		borrowedBook.setDateCreated(DateHelper.getTodayWithTime());
		borrowedBook.setModifiedBy(null);
		borrowedBook.setDateModified(null);
		returnObj = borrowedBookRepository.save(borrowedBook);
		return returnObj;
	}

	@Override
	public BorrowedBook update(BorrowedBook borrowedBook) {
		BorrowedBook returnObj = null;
		borrowedBook.setDateModified(DateHelper.getTodayWithTime());
		returnObj = borrowedBookRepository.save(borrowedBook);
		return returnObj;
	}

	@Override
	public BorrowedBook findOne(long id) {
		BorrowedBook returnObj = null;
		returnObj = borrowedBookRepository.findById(id);
		return returnObj;
	}

	@Override
	public List<BorrowedBook> save(List<BorrowedBook> borrowedBooks) {
		List<BorrowedBook> returnObj = null;
		borrowedBooks.stream().forEach(b -> {
			b.setDateCreated(DateHelper.getTodayWithTime());
			b.setDateModified(null);
		});
		returnObj = borrowedBookRepository.save(borrowedBooks);
		return returnObj;
	}

	@Override
	public List<BorrowedBook> update(List<BorrowedBook> borrowedBooks) {
		List<BorrowedBook> returnObj = null;
		borrowedBooks.stream().forEach(b -> b.setDateModified(DateHelper.getTodayWithTime()));
		returnObj = borrowedBookRepository.save(borrowedBooks);
		return returnObj;
	}

	@Override
	public List<BorrowedBook> findAll() {
		List<BorrowedBook> borrowedBooks = null;
		borrowedBooks = borrowedBookRepository.findAll();
		return borrowedBooks;
	}

	@Override
	public List<BorrowedBook> findAll(Specification<BorrowedBook> borrowedBookSpecification) {
		List<BorrowedBook> borrowedBooks = null;
		borrowedBooks = borrowedBookRepository.findAll(borrowedBookSpecification);
		return borrowedBooks;
	}

	@Override
	public void delete(long id) {
		borrowedBookRepository.delete(id);
	}

	@Override
	public void deleteInBatch(List<BorrowedBook> borrowedBooks) {
		borrowedBookRepository.deleteInBatch(borrowedBooks);
	}

}
