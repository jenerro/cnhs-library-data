package libdata.serviceImplemention;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import libdata.model.User;
import libdata.repository.UserRepository;
import libdata.service.UserService;
import libdata.util.DateHelper;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@Service
@Transactional
public class UserServiceImp implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User save(User user) {
		User returnObj = null;
		user.setDateCreated(DateHelper.getTodayWithTime());
		user.setModifiedBy(null);
		user.setDateModified(null);
		returnObj = userRepository.save(user);
		return returnObj;
	}

	@Override
	public User update(User user) {
		User returnObj = null;
		user.setDateModified(DateHelper.getTodayWithTime());
		returnObj = userRepository.save(user);
		return returnObj;
	}

	@Override
	public User findOne(long id) {
		User returnObj = null;
		returnObj = userRepository.findById(id);
		return returnObj;
	}

	@Override
	public List<User> save(List<User> users) {
		List<User> returnObj = null;
		users.stream().forEach(b -> {
			b.setDateCreated(DateHelper.getTodayWithTime());
			b.setDateModified(null);
		});
		returnObj = userRepository.save(users);
		return returnObj;
	}

	@Override
	public List<User> update(List<User> users) {
		List<User> returnObj = null;
		users.stream().forEach(b -> b.setDateModified(DateHelper.getTodayWithTime()));
		returnObj = userRepository.save(users);
		return returnObj;
	}

	@Override
	public List<User> findAll() {
		List<User> users = null;
		users = userRepository.findAll();
		return users;
	}

	@Override
	public List<User> findAll(Specification<User> userSpecification) {
		List<User> users = null;
		users = userRepository.findAll(userSpecification);
		return users;
	}

	@Override
	public void delete(long id) {
		userRepository.delete(id);
	}

	@Override
	public void deleteInBatch(List<User> users) {
		userRepository.deleteInBatch(users);
	}

}
