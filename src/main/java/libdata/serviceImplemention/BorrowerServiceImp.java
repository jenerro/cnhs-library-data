package libdata.serviceImplemention;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import libdata.model.Borrower;
import libdata.repository.BorrowerRepository;
import libdata.service.BorrowerService;
import libdata.util.DateHelper;

/**
 * @author Ace Jenerro Baluyot
 *
 */
@Service
@Transactional
public class BorrowerServiceImp implements BorrowerService {

	@Autowired
	private BorrowerRepository borrowerRepository;

	@Override
	public Borrower save(Borrower borrower) {
		Borrower returnObj = null;
		borrower.setDateCreated(DateHelper.getTodayWithTime());
		borrower.setModifiedBy(null);
		borrower.setDateModified(null);
		returnObj = borrowerRepository.save(borrower);
		return returnObj;
	}

	@Override
	public Borrower update(Borrower borrower) {
		Borrower returnObj = null;
		borrower.setDateModified(DateHelper.getTodayWithTime());
		returnObj = borrowerRepository.save(borrower);
		return returnObj;
	}

	@Override
	public Borrower findOne(long id) {
		Borrower returnObj = null;
		returnObj = borrowerRepository.findById(id);
		return returnObj;
	}

	@Override
	public List<Borrower> save(List<Borrower> borrowers) {
		List<Borrower> returnObj = null;
		borrowers.stream().forEach(b -> {
			b.setDateCreated(DateHelper.getTodayWithTime());
			b.setDateModified(null);
		});
		returnObj = borrowerRepository.save(borrowers);
		return returnObj;
	}

	@Override
	public List<Borrower> update(List<Borrower> borrowers) {
		List<Borrower> returnObj = null;
		borrowers.stream().forEach(b -> b.setDateModified(DateHelper.getTodayWithTime()));
		returnObj = borrowerRepository.save(borrowers);
		return returnObj;
	}

	@Override
	public List<Borrower> findAll() {
		List<Borrower> borrowers = null;
		borrowers = borrowerRepository.findAll();
		return borrowers;
	}

	@Override
	public List<Borrower> findAll(Specification<Borrower> borrowerSpecification) {
		List<Borrower> borrowers = null;
		borrowers = borrowerRepository.findAll(borrowerSpecification);
		return borrowers;
	}

	@Override
	public void delete(long id) {
		borrowerRepository.delete(id);
	}

	@Override
	public void deleteInBatch(List<Borrower> borrowers) {
		borrowerRepository.deleteInBatch(borrowers);
	}

}
